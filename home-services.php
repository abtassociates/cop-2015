<!-- service section -->
<?php $wl_theme_options = weblizar_get_options(); ?>
<div class="enigma_service">
<?php if($wl_theme_options['home_service_heading'] !='') { ?>
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<div class="enigma_heading_title">
				<h3><?php echo esc_attr($wl_theme_options['home_service_heading']); ?></h3>		
			</div>
		</div>
	</div>
</div>	
<?php } ?>
<div class="container">
		<div class="row isotope" id="isotope-service-container">		
        
        <div align="center"> 
        	<!-- start pictureFill -->
            
<a href="http://cop2015.abtassociates.com/agendas/monday-june-8/">
<picture>
<!--[if IE 9]><div style="display: none;"><![endif]-->
    <source srcset="wp-content/themes/cop-2015/images/blocks-monday-mobile.png" media="(max-width: 800px)">         
    <img src="wp-content/themes/cop-2015/images/blocks-monday.png" alt="Monday, June 8, New COP Orientation">
 <!--[if IE 9]></div><![endif]-->
</picture>
</a>

<a href="http://cop2015.abtassociates.com/agendas/tuesday-june-9/">
<picture>
          <!--[if IE 9]><div style="display: none;"><![endif]-->
    		<source srcset="wp-content/themes/cop-2015/images/blocks-tuesday-mobile.png" media="(max-width: 800px)"> 
          <!--[if IE 9]></div><![endif]-->
            <img src="wp-content/themes/cop-2015/images/blocks-tuesday.png" alt="Tuesday, June 9, Impact and Innovation">
</picture>
</a>

<a href="http://cop2015.abtassociates.com/agendas/wednesday-june-10/">
<picture>
          <!--[if IE 9]><div style="display: none;"><![endif]-->
            <source srcset="wp-content/themes/cop-2015/images/blocks-wednesday-mobile.png" media="(max-width: 800px)">
          <!--[if IE 9]></div><![endif]-->
            <img src="wp-content/themes/cop-2015/images/blocks-wednesday.png" alt="Wednesday, June 10, Compliance and Breakouts">
</picture>
</a>
<a href="http://cop2015.abtassociates.com/agendas/thursday-june-11/">
<picture>
          <!--[if IE 9]><div style="display: none;"><![endif]-->
    		<source srcset="wp-content/themes/cop-2015/images/blocks-thursday-mobile.png" media="(max-width: 800px)">
          <!--[if IE 9]></div><![endif]-->
            <img src="wp-content/themes/cop-2015/images/blocks-thursday.png" alt="Thursday, June 11, Evidence to Action">
</picture>
</a>
</div><!-- end pictureFill-->

        </div>
	</div>
</div>	 
<!-- /Service section -->
</div><!--closing bg png; opens in homeslideshow php-->