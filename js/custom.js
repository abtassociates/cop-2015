
/* For collapse feature on General Conference Information page*/
new jQueryCollapse(jQuery("#generalCollapse"), {
  open: function() {
	this.slideDown(150);
  },
  close: function() {
	this.slideUp(150);
  }
}); 